package com.gildedrose;

import java.io.FileWriter;
import java.io.IOException;

public class TexttestFixture {
    public static void main(String[] args) {
        String output = goldenMasterExecution();
        // output in file
        try(FileWriter fileWriter = new FileWriter("/home/toinof/dev/katas/GildedRose-Refactoring-Kata/Java/src/test/resources/golden-master.out")) {
            fileWriter.write(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String goldenMasterExecution() {
        Item[] items = new Item[]{
                Item.createItem("+5 Dexterity Vest", 10, 20), //
                Item.createItem("Aged Brie", 2, 0), //
                Item.createItem("Elixir of the Mongoose", 5, 7), //
                Item.createItem("Sulfuras, Hand of Ragnaros", 0, 80), //
                Item.createItem("Sulfuras, Hand of Ragnaros", -1, 80),
                Item.createItem("Backstage passes to a TAFKAL80ETC concert", 15, 20),
                Item.createItem("Backstage passes to a TAFKAL80ETC concert", 10, 49),
                Item.createItem("Backstage passes to a TAFKAL80ETC concert", 5, 49),
                // this conjured item does not work properly yet
                Item.createItem("Conjured Mana Cake", 3, 6)};

        GildedRose app = new GildedRose(items);

        int days = 25;

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < days; i++) {
            stringBuilder.append("-------- day ").append(i).append(" --------")
                    .append(System.lineSeparator())
                    .append("name, sellIn, quality")
                    .append(System.lineSeparator());
            for (Item item : items) {
                stringBuilder.append(item)
                        .append(System.lineSeparator());

            }
            stringBuilder.append(System.lineSeparator());
            app.updateQuality();
        }

        return stringBuilder.toString();
    }

}
