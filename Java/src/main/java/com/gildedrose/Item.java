package com.gildedrose;

public class Item {

    public static final String AGED_BRIE = "Aged Brie";
    public static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
    public String name;

    public int sellIn;

    public int quality;

    protected Item(String name, int sellIn, int quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }

    public static Item createItem(String name, int sellIn, int quality) {
        if (isRegularItem(name)) {
            return new RegularItem(name, sellIn, quality);
        }
        if (AGED_BRIE.equals(name)) {
            return new AgedBrieItem(name, sellIn, quality);
        }
        if (name.equals(BACKSTAGE_PASSES)) {
            return new BackstageItem(name, sellIn, quality);
        }

        return new SulfurasItem(name, sellIn, quality);
    }

    @Override
    public String toString() {
        return this.name + ", " + this.sellIn + ", " + this.quality;
    }

    void updateBackstagePassesQuality() {
        if (quality < 50) {
            quality = quality + 1;
            if (sellIn < 11) {
                if (quality < 50) {
                    quality = quality + 1;
                }
            }
            if (sellIn < 6) {
                if (quality < 50) {
                    quality = quality + 1;
                }
            }
        }
        sellIn = sellIn - 1;
        if (sellIn < 0) {
            quality = 0;
        }
    }

    void updateAgedBrieQuality() {
        if (quality < 50) {
            quality = quality + 1;
        }
        sellIn = sellIn - 1;
        if (sellIn < 0) {
            if (quality < 50) {
                quality = quality + 1;
            }
        }
    }

    void ageRegularItem() {
        if (quality > 0) {
            quality = quality - 1;
        }
        sellIn = sellIn - 1;
        if (sellIn < 0) {
            if (quality > 0) {
                quality = quality - 1;
            }
        }
    }

    private static boolean isRegularItem(String name) {
        return !name.equals(AGED_BRIE) && !name.equals(BACKSTAGE_PASSES) && !name.equals(SULFURAS);
    }

    boolean isRegularItem() {
        return !name.equals(AGED_BRIE) && !name.equals(BACKSTAGE_PASSES) && !name.equals(SULFURAS);
    }

    public void ageItem() {
    }
}
