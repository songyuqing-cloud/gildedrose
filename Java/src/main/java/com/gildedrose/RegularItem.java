package com.gildedrose;

public class RegularItem extends Item {
    protected RegularItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    public void ageItem() {
        ageRegularItem();
    }
}
